/**
 * Created by honcharov_victor on 29.03.17.
 */
(function(){


    var app = angular.module('System.Views');

    app.config([ '$stateProvider' , '$urlRouterProvider' , function( $stateProvider , $urlRouterProvider ){

        $stateProvider.state({

            name: 			'config',

            url: 			'^/config',

            templateUrl: 	'components/config/views/config.html',

            controller: 	'ConfigController',

            breadcrumbs: 	[ { icon:'home' , url:'/config', name: 'Config' } ]

        });

    }]);


})();