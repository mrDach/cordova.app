/**
 * Created by honcharov_victor on 29.03.17.
 */
(function(){

    'use strict';

    var app = angular.module('System.Controllers');

    //The App Controller
    app.controller('ConfigController', [ '$scope' ,'DataService' , function( $scope, DataService ){

        $scope.config = {};

        DataService.get('Config').then(function (response) {

            if (!response.data.result) {

                $scope.errors = response.data.errors;

            } else {

                $scope.config = response.data.data;

            }
        });

        $scope.submit = function (form) {

            DataService.set('Config',$scope.config).then(function (response) {

                if (!response.data.result) {

                    $scope.errors = response.data.errors;

                } else {

                    $scope.merchant = response.data.data;

                }
            });

        };

    }]);

})();