/**
 * Created by honcharov_victor on 22.04.17.
 */
(function(){

    'use strict';

    var app 		= angular.module('System.Directives');
    /**
     *
     *	DIRECTIVE: 	paginationLimit
     *		- Creates the Pagination Dropdown
     *=
     *
     * 	USAGE:
     * 		<pagination-limit></pagination-limit>
     *
     **/
    app.directive('paginationLimit', [ '$location' , function( $location ){
        return {
            restrict: 'E',

            transclude: true,

            templateUrl: 'components/pagination/views/paginationLimit.html',

            link: function( $scope , $element , $attributes ){

                $scope.page.showing = ( $location.search().limit || 15 );

                $scope.changePaginationLimit = function( limit ){

                    $scope.page.showing = limit;
                    $scope.page.current = 1;

                    $location.search({ 'limit' : limit });

                }

            }

        }
    }]);

})();