/**
 * Created by honcharov_victor on 22.04.17.
 */
(function(){

    'use strict';

    var app = angular.module('System.Controllers');

    //The App Controller
    app.controller('SourceEditController', ['$scope', 'DataService', '$stateParams', function ($scope, DataService, $stateParams) {

        $scope.page = {
            "title": "Source",
            "icon": "plus-circle",
            "action": "Add",
            "sending": false
        };

        $scope.morph_type = {
            "id":""
        };

        $scope.sourceModel = {
            "name": '',
            "morph_type": $scope.morph_type.id
        };

        $scope.source = angular.copy($scope.sourceModel);

        $scope.types = {
            data: [
                {"id":"","name":"None"},
                {"id":"privat","name":"Privat24"}
            ],
            settings: {
                displayProp: 'name',
                smartButtonMaxItems: 1,
                closeOnSelect: true,
                showUncheckAll: false,
                selectionLimit: 1,
                smartButtonTextConverter: function (itemText, originalItem) {
                    return itemText;
                }
            },
            events: {
                onItemSelect: function () {
                    $scope.operationForm.sources.$dirty = $scope.source.morph_type;
                },
                onItemDeselect: function () {
                    $scope.operationForm.sources.$dirty = $scope.source.morph_type;
                },
                onSelectAll: function () {
                    $scope.operationForm.sources.$dirty = $scope.source.morph_type;
                },
                onDeselectAll: function () {
                    $scope.operationForm.sources.$dirty = false;
                }
            }
        };

        if ($stateParams.sourceId) {
            $scope.page.icon = "pencil-square";
            $scope.page.action = "Edit";
            $scope.page.sending = true;
            DataService.get('Source', $stateParams.sourceId).then(function (response) {

                $scope.page.sending = false;

                if (!response.data.result) {

                    $scope.errors = response.data.errors;

                } else {

                    $scope.errors = [];
                    $scope.source = response.data.data;
                    $scope.morph_type.id = $scope.source.morph_type;

                }

            });
        }

        $scope.submit = function (form) {

            if (form.$valid && !$scope.page.sending) {

                $scope.source.morph_type = $scope.morph_type.id;
                if(!$scope.source.morph_type){
                    delete $scope.source.morph_id;
                    delete $scope.source.morph;
                }
                $scope.page.sending = true;
                DataService.set('Source', $scope.source).then(function (response) {

                    $scope.page.sending = false;

                    if (!response.data.result) {

                        $scope.errors = response.data.errors;

                    } else {

                        $scope.errors = [];
                        $scope.source = angular.copy($scope.sourceModel);
                        form.$setPristine();

                    }

                });

            }

        }

    }]);

})();