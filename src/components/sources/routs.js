/**
 * Created by honcharov_victor on 29.03.17.
 */
(function(){

    var app = angular.module('System.Views');
    app.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {

        $stateProvider.state({

            name: 'source-add',

            url: '^/source/add',

            templateUrl: 'components/sources/views/edit.html',

            controller: 'SourceEditController',

            breadcrumbs: [
                {icon: 'home', url: '/', name: 'Dashboard'},
                {icon: '', url: '/source', name: 'Sources'}
            ]

        }).state({

            name: 'source-edit',

            url: '^/source/edit/{sourceId:int}',

            params: {page: {value: null, squash: true}},

            templateUrl: 'components/sources/views/edit.html',

            controller: 'SourceEditController',

            breadcrumbs: [
                {icon: 'home', url: '/', name: 'Dashboard'},
                {icon: '', url: '/source', name: 'Source'},
                {icon: '', url: '/source/edit/:sourceId', name: 'Edit Source'}
            ]

        }).state({

            name: 'source-list',

            url: '^/source',

            templateUrl: 'components/sources/views/list.html',

            controller: 'ListController',

            breadcrumbs: [
                {icon: 'home', url: '/', name: 'Dashboard'},
                {icon: '', url: '/source', name: 'Source'}
            ],

            model: 'Source'

        });

    }]);

})();