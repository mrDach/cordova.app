/**
 * Created by honcharov_victor on 29.03.17.
 */
(function(){


    var app = angular.module('System.Views');

    app.config([ '$stateProvider' , '$urlRouterProvider' , function( $stateProvider , $urlRouterProvider ){

        $stateProvider.state({

            name: 			'products-add',

            url: 			'^/products/add',

            templateUrl: 	':api/layout/controllers/manage/user',

            controller: 	'UserManagementController',

            breadcrumbs: 	[ { icon:'home' , url:'/', name: 'Dashboard' } , { icon:'' , url: '/products' , name: 'products' } , { icon:'' , url: '/products/add' , name: 'Add User' } ]

        }).state({

            name: 			'products-edit',

            url: 			'^/products/edit/{userid:int}',

            params: 		{ page: { value:null, squash:true } },

            templateUrl: 	':api/layout/controllers/manage/user',

            controller: 	'UserManagementController',

            breadcrumbs: 	[ { icon:'home' , url:'/', name: 'Dashboard' } , { icon:'' , url: '/products' , name: 'products' } , { icon:'' , url: '/products/edit/:productid' , name: 'Edit Product' } ]

        }).state({

            name: 			'products-list',

            url: 			'^/products',

            templateUrl: 	':api/layout/controllers/user',

            controller: 	'UserManagementController',

            breadcrumbs: 	[ { icon:'home' , url:'/', name: 'Dashboard' } , { icon:'' , url: '/products' , name: 'products' } ]

        });

    }]);


})();