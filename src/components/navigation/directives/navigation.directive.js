/**
 * Created by honcharov_victor on 28.03.17.
 */
(function(){
    var app 		= angular.module('System.Directives');
    /**
     *
     *	DIRECTIVE: 	percentage
     *		- Format a Field into a Percentage
     *
     * 	USAGE:
     * 		[percentage]
     *
     **/
    app.directive('navigation', navigation);
    navigation.$inject = ['$location', '$rootScope'];
    function navigation($location, $rootScope){
        return {
            restrict: 'E',
            transclude: true,
            templateUrl: 'components/navigation/views/navigation.html',
            link : function (scope, element) {

                scope.is        = is;
                scope.open      = open;
                scope.open      = back;
                scope.selected  = selected;
                scope.toggleMainMenu  = toggleMainMenu;

                // TOGGLE CLASS on click
                // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
                function toggleClassOnClick(object) {

                    var target = object.attr('data-target');
                    var toggleClass = object.attr('data-toggle-class');

                    object.on('click.toggleClass.fireEvent', function (e) {

                        e.preventDefault();
                        $(target).toggleClass(toggleClass);
                    });
                }
                $('[data-toggle-class][data-target]').each(function () {
                    toggleClassOnClick($(this));
                });

                //NAVIGATION LEFT-SIDEBAR
                // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
                function openItemLeftSidbar(item) {
                    item.children('ul.child-nav').slideDown(500, function () {
                        $(this).css('display', '');
                    });
                    item.addClass('open-item').removeClass('close-item');
                }

                function closeItemLeftSidbar(item) {
                    item.children('ul.child-nav').slideUp(300, function () {
                        $(this).css('display', '');
                        item.addClass('close-item').removeClass('open-item');
                    });
                }

                var main_nav = $('#main-nav');

                //OPEN NAV ITEMS
                //-------------------------------------------------------------------
                main_nav.on('click', 'li.close-item > a', function () {
                    var parent = $(this).parent('li');
                    openItemLeftSidbar(parent);

                    parent.siblings('li.open-item').each(function () {
                        closeItemLeftSidbar($(this));
                    });
                });
                //CLOSE NAV ITEMS
                //-------------------------------------------------------------------
                main_nav.on('click', 'li.open-item > a', function () {
                    var parent = $(this).parent('li');
                    closeItemLeftSidbar(parent);
                });

                /**
                 * Compares the current Path with the passed Regex
                 * @param regex {String} regex for test
                 * @return {bool}
                 */
                function is( regex ){

                    return $location.path().match( regex );

                }

                /**
                 * On Click, Run the Page Opening function
                 * @param destination {String} The Route to Open
                 */
                function open( destination ){
                    toggleMainMenu();
                    //Clear all Unnecessary AJAX Queues
                    //Loader.clear();

                    //Go to Location
                    $location.url( destination );

                }

                /**
                 * On Click, Open the Previous Page or Fallback Page
                 * @param fallback {String} The fallback Destination
                 */
                function back( fallback ){

                    var fallback = fallback || '/';

                    //Only delete it if we have more than 1 page
                    if( $rootScope.history.length > 1 ){

                        //Get the Previous Page
                        $rootScope.history.pop();

                        //Get the Last Page
                        var $previous = $rootScope.history[ $rootScope.history.length - 1 ];

                        //Open the Previous Page
                        open($previous);

                    }

                    //Just open the Fallback
                    open( fallback );

                }

                /**
                 * Check if the current navigation item is selected
                 * @param link {String} The Link to Validate
                 * @param active {String} The Classes to set on Success
                 * @param inactive {String} The Classes to set on Failure
                 * @return {String} The Provided active or inactive string
                 */
                function selected( link , active , inactive ){

                    var location = ( $location.path() || '/' );

                    if( location == link.replace(/\*/,'') || ( link.length > 1 && link.indexOf('*') > -1 && new RegExp( '^' + link ).test( location ) ) ){

                        //Return the Active Class
                        return active;

                    }

                    //Return the Inactive Class
                    return inactive;
                }

                /**
                 * Toggle navigation menu (hide or show)
                 */
                function toggleMainMenu() {
                    if ($rootScope.hideMainMenu == undefined || $rootScope.hideMainMenu == false) {
                        $rootScope.hideMainMenu = true;
                    } else if ($rootScope.hideMainMenu == true) {
                        $rootScope.hideMainMenu = false;
                    }
                }

            }

        }

    }
})();