/**
 * Created by honcharov_victor on 22.04.17.
 */
(function(){

    'use strict';

    var app = angular.module('System.Controllers');

    //The App Controller
    app.controller('OperationEditController', ['$scope', 'DataService', '$stateParams', function ($scope, DataService, $stateParams) {

        $scope.page = {
            "title": "Operation",
            "icon": "plus-circle",
            "action": "Add",
            "sending": false
        };

        $scope.operationModel = {
            "source": {},
            "date": new Date(),
            "amount": 0,
            "product_list": {}
        };

        $scope.operation = angular.copy($scope.operationModel);

        $scope.openCalendar = function() {
            $scope.datepickerOptions.open = true;
        };

        $scope.datepickerOptions = {
            date: $scope.operation.date,
            open: false
        };

        DataService.get('SourceList', {"limit":0}).then(function (response) {

            $scope.sources = {
                data: response.data.data,
                settings: {
                    displayProp: 'name',
                    smartButtonMaxItems: 1,
                    closeOnSelect: true,
                    showUncheckAll: false,
                    selectionLimit: 1,
                    smartButtonTextConverter: function (itemText, originalItem) {
                        return itemText;
                    }
                },
                events: {
                    onItemSelect: function () {
                        $scope.operationForm.sources.$dirty = $scope.operation.source.length;
                    },
                    onItemDeselect: function () {
                        $scope.operationForm.sources.$dirty = $scope.operation.source.length;
                    },
                    onSelectAll: function () {
                        $scope.operationForm.sources.$dirty = $scope.operation.source.length;
                    },
                    onDeselectAll: function () {
                        $scope.operationForm.sources.$dirty = false;
                    }
                }
            };

        });

        DataService.get('ProductListList', {"limit":0}).then(function (response) {

            $scope.productList = {
                data: response.data.data,
                settings: {
                    displayProp: 'name',
                    smartButtonMaxItems: 1,
                    closeOnSelect: true,
                    showUncheckAll: false,
                    selectionLimit: 1,
                    smartButtonTextConverter: function (itemText, originalItem) {
                        return itemText;
                    }
                }
            };

        });

        if ($stateParams.operationId) {
            $scope.page.icon = "pencil-square";
            $scope.page.action = "Edit";
            $scope.page.sending = true;
            DataService.get('Operation', $stateParams.operationId).then(function (response) {

                $scope.page.sending = false;

                if (!response.data.result) {

                    $scope.errors = response.data.errors;

                } else {

                    $scope.errors = [];
                    $scope.operation = response.data.data;

                    $scope.operation.amount = parseFloat($scope.operation.amount);
                    if($scope.operation.product_list == null){
                        $scope.operation.product_list = {};
                    }

                }

            });
        }

        $scope.submit = function (form) {

            if (form.$valid && !$scope.page.sending) {

                $scope.page.sending = true;
                DataService.set('Operation', $scope.operation).then(function (response) {

                    $scope.page.sending = false;

                    if (!response.data.result) {

                        $scope.errors = response.data.errors;

                    } else {

                        $scope.errors = [];
                        if($scope.operation.id == undefined){
                            $scope.operation = angular.copy($scope.operationModel);
                            form.$setPristine();
                        }

                    }

                });

            }

        }

    }]);

})();