/**
 * Created by honcharov_victor on 29.03.17.
 */
(function(){

    var app = angular.module('System.Views');
    app.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {

        $stateProvider.state({

            name: 'operation-add',

            url: '^/operation/add',

            templateUrl: 'components/operations/views/edit.html',

            controller: 'OperationEditController',

            breadcrumbs: [
                {icon: 'home', url: '/', name: 'Dashboard'},
                {icon: '', url: '/operation', name: 'Operations'}
            ]

        }).state({

            name: 'operation-edit',

            url: '^/operation/edit/{operationId:int}',

            params: {page: {value: null, squash: true}},

            templateUrl: 'components/operations/views/edit.html',

            controller: 'OperationEditController',

            breadcrumbs: [
                {icon: 'home', url: '/', name: 'Dashboard'},
                {icon: '', url: '/operation', name: 'Operation'},
                {icon: '', url: '/operation/edit/:operationId', name: 'Edit Operation'}
            ]

        }).state({

            name: 'operation-list',

            url: '^/operation',

            templateUrl: 'components/operations/views/list.html',

            controller: 'ListController',

            breadcrumbs: [
                {icon: 'home', url: '/', name: 'Dashboard'},
                {icon: '', url: '/operation', name: 'Operation'}
            ],

            model: 'Operation'

        });

    }]);

})();