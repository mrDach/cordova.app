/**
 * Created by honcharov_victor on 22.04.17.
 */
(function(){

    'use strict';

    var app = angular.module('System.Controllers');

    //The App Controller
    app.controller('ProductListEditController', ['$scope', 'DataService', '$stateParams', function ($scope, DataService, $stateParams) {

        $scope.page = {
            "title": "Product List",
            "icon": "plus-circle",
            "action": "Add",
            "sending": false
        };

        $scope.productModel = {
            "name":"",
            "cost":"",
            "amount":"",
            "category":{}
        };

        $scope.productListModel = {
            "name": '',
            "date": new Date(),
            "products": [angular.copy($scope.productModel)]
        };

        $scope.productList = angular.copy($scope.productListModel);

        $scope.openCalendar = function() {
            $scope.datepickerOptions.open = true;
        };

        $scope.datepickerOptions = {
            date: $scope.productList.date,
            open: false
        };

        DataService.get('ProductCategoryList', {"limit":0}).then(function (response) {

            $scope.productCategorys = {
                data: response.data.data,
                settings: {
                    displayProp: 'name',
                    smartButtonMaxItems: 1,
                    closeOnSelect: true,
                    showUncheckAll: false,
                    selectionLimit: 1,
                    smartButtonTextConverter: function (itemText, originalItem) {
                        return itemText;
                    }
                }
            };

        });

        if ($stateParams.productListId) {
            $scope.page.sending = true;
            DataService.get('ProductList', $stateParams.productListId).then(function (response) {

                $scope.page.sending = false;

                if (!response.data.result) {

                    $scope.errors = response.data.errors;

                } else {

                    $scope.errors = [];
                    $scope.productList = response.data.data;

                    if($scope.productList.products.length == 0){
                        $scope.productList.products = [angular.copy($scope.productModel)];
                    }

                }

            });
        }

        $scope.submit = function (form) {
            //if (form.$valid && !$scope.page.sending) {
                $scope.page.sending = true;
                DataService.set('ProductList', $scope.productList).then(function (response) {
                    $scope.page.sending = false;
                    if (!response.data.result) {
                        $scope.errors = response.data.errors;
                    } else {
                        $scope.errors = [];
                        $scope.productList = angular.copy($scope.productListModel);
                        form.$setPristine();
                    }
                });
            //}
        };

        $scope.pushProduct = function () {
            $scope.productList.products.push(angular.copy($scope.productModel));
        };

        $scope.pullProduct = function (index) {
            $scope.productList.products.splice(index,1);
        };

    }]);

})();