/**
 * Created by honcharov_victor on 29.03.17.
 */
(function(){

    var app = angular.module('System.Views');
    app.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {

        $stateProvider.state({

            name: 'productLists-add',

            url: '^/product/product_list/add',

            templateUrl: 'components/productLists/views/edit.html',

            controller: 'ProductListEditController',

            breadcrumbs: [
                {icon: 'home', url: '/', name: 'Dashboard'},
                {icon: '', url: '/products', name: 'products'},
                {icon: '', url: '/products/add', name: 'Add User'}
            ]

        }).state({

            name: 'productLists-edit',

            url: '^/product/product_list/edit/{productListId:int}',

            params: {page: {value: null, squash: true}},

            templateUrl: 'components/productLists/views/edit.html',

            controller: 'ProductListEditController',

            breadcrumbs: [
                {icon: 'home', url: '/', name: 'Dashboard'},
                {icon: '', url: '/products', name: 'products'},
                {icon: '', url: '/products/edit/:productListId', name: 'Edit Product List'}
            ]

        }).state({

            name: 'productLists-list',

            url: '^/product/product_list',

            templateUrl: 'components/productLists/views/list.html',

            controller: 'ListController',

            breadcrumbs: [
                {icon: 'home', url: '/', name: 'Dashboard'},
                {icon: '', url: '/products', name: 'products'}
            ],

            model: 'ProductList'

        });

    }]);

})();