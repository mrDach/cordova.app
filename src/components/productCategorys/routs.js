/**
 * Created by honcharov_victor on 29.03.17.
 */
(function(){

    var app = angular.module('System.Views');
    app.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {

        $stateProvider.state({

            name: 'productCategorys-add',

            url: '^/product/category/add',

            templateUrl: 'components/productCategorys/views/edit.html',

            controller: 'ProductCategoryEditController',

            breadcrumbs: [
                {icon: 'home', url: '/', name: 'Dashboard'},
                {icon: '', url: '/products', name: 'products'},
                {icon: '', url: '/products/add', name: 'Add User'}
            ]

        }).state({

            name: 'productCategorys-edit',

            url: '^/product/category/edit/{categoryId:int}',

            params: {page: {value: null, squash: true}},

            templateUrl: 'components/productCategorys/views/edit.html',

            controller: 'ProductCategoryEditController',

            breadcrumbs: [
                {icon: 'home', url: '/', name: 'Dashboard'},
                {icon: '', url: '/products', name: 'products'},
                {icon: '', url: '/products/edit/:categoryId', name: 'Edit Product Category'}
            ]

        }).state({

            name: 'productCategorys-list',

            url: '^/product/category',

            templateUrl: 'components/productCategorys/views/list.html',

            controller: 'ListController',

            breadcrumbs: [
                {icon: 'home', url: '/', name: 'Dashboard'},
                {icon: '', url: '/products', name: 'products'}
            ],

            model: 'ProductCategory'

        });

    }]);

})();