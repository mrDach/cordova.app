/**
 * Created by honcharov_victor on 22.04.17.
 */
(function(){

    'use strict';

    var app = angular.module('System.Controllers');

    //The App Controller
    app.controller('ProductCategoryEditController', ['$scope', 'DataService', '$stateParams', function ($scope, DataService, $stateParams) {

        $scope.page = {
            "title": "Category",
            "icon": "plus-circle",
            "action": "Add",
            "sending": false
        };

        $scope.categoryModel = {
            "name": ''
        };

        $scope.category = angular.copy($scope.categoryModel);

        if ($stateParams.categoryId) {
            $scope.page.icon = "pencil-square";
            $scope.page.action = "Edit";
            $scope.page.sending = true;
            DataService.get('ProductCategory', $stateParams.categoryId).then(function (response) {

                $scope.page.sending = false;

                if (!response.data.result) {

                    $scope.errors = response.data.errors;

                } else {

                    $scope.category = response.data.data;

                }

            });
        }

        $scope.submit = function (form) {

            if (form.$valid && !$scope.page.sending) {

                $scope.page.sending = true;
                DataService.set('ProductCategory', $scope.category).then(function (response) {

                    $scope.page.sending = false;

                    if (!response.data.result) {

                        $scope.errors = response.data.errors;

                    } else {

                        $scope.category = angular.copy($scope.categoryModel);
                        form.$setPristine();

                    }

                });

            }

        }

    }]);

})();