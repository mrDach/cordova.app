/**
 * Created by honcharov_victor on 17.04.17.
 */
(function(){

    var app 		= angular.module('System.Services');

    /**
     *
     *	DataService
     *		- Checks each outbound router for :api and replaces it with the correct API path
     *
     *
     **/

    app.factory('RemoteDataService', [ '$http', function( $http ){

        var LocalDataService = {
            getConfig: getConfig,
            setConfig: setConfig,

            getProductCategoryList: getProductCategoryList,
            getProductCategory: getProductCategory,
            setProductCategory: setProductCategory,
            deleteProductCategory: deleteProductCategory,

            getSourceList: getSourceList,
            getSource: getSource,
            setSource: setSource,
            deleteSource: deleteSource,
            setGrabSource: setGrabSource,

            getOperationList: getOperationList,
            getOperation: getOperation,
            setOperation: setOperation,
            deleteOperation: deleteOperation,

            getProductListList: getProductListList,
            getProductList: getProductList,
            setProductList: setProductList,
            deleteProductList: deleteProductList,
        };

        function getConfig() {
            return $http.get(':api/user/config');
        }

        function setConfig(value) {
            return $http.post(':api/user/config',value);
        }

        function getProductCategoryList(parameters) {
            var parameters = parameters||{};
            var limit = (parameters.limit === 0)?parameters.limit:(parameters.limit||15),
                page  = parameters.page||1;
            return $http.get(':api/product/category/' + limit + '/' + page);
        }

        /**
         *
         * @param categoryId
         * @return {HttpPromise}
         */
        function getProductCategory(categoryId) {
            return $http.get(':api/product/category/'+categoryId);
        }

        /**
         *
         * @param categoryObj
         * @return {HttpPromise}
         */
        function setProductCategory(categoryObj) {
            return $http.post(':api/product/category',categoryObj);
        }

        /**
         *
         * @param categoryId
         * @return {HttpPromise}
         */
        function deleteProductCategory(categoryId) {
            return $http.post(':api/product/category/'+categoryId);
        }

        function getSourceList(parameters) {
            var parameters = parameters||{};
            var limit = (parameters.limit === 0)?parameters.limit:(parameters.limit||15),
                page  = parameters.page||1;
            return $http.get(':api/source/' + limit + '/' + page);
        }

        /**
         *
         * @param sourceId
         * @return {HttpPromise}
         */
        function getSource(sourceId) {
            return $http.get(':api/source/'+sourceId);
        }

        /**
         *
         * @param sourceObj
         * @return {HttpPromise}
         */
        function setSource(sourceObj) {
            return $http.post(':api/source',sourceObj);
        }

        /**
         *
         * @param sourceId
         * @return {HttpPromise}
         */
        function deleteSource(sourceId) {
            return $http.post(':api/source/'+sourceId);
        }

        /**
         *
         * @param sourceId
         * @return {HttpPromise}
         */
        function setGrabSource(sourceId) {
            return $http.post(':api/source/'+sourceId+'/grab');
        }

        function getOperationList(parameters) {
            var parameters = parameters||{};
            var limit = (parameters.limit === 0)?parameters.limit:(parameters.limit||15),
                page  = parameters.page||1;
            return $http.get(':api/operation/' + limit + '/' + page);
        }

        /**
         *
         * @param operationId
         * @return {HttpPromise}
         */
        function getOperation(operationId) {
            return $http.get(':api/operation/'+operationId);
        }

        /**
         *
         * @param operationObj
         * @return {HttpPromise}
         */
        function setOperation(operationObj) {
            return $http.post(':api/operation',operationObj);
        }

        /**
         *
         * @param operationId
         * @return {HttpPromise}
         */
        function deleteOperation(operationId) {
            return $http.post(':api/operation/'+operationId);
        }

        function getProductListList(parameters) {
            var parameters = parameters||{};
            var limit = (parameters.limit === 0)?parameters.limit:(parameters.limit||15),
                page  = parameters.page||1;
            return $http.get(':api/product/list/' + limit + '/' + page);
        }

        /**
         *
         * @param productListId
         * @return {HttpPromise}
         */
        function getProductList(productListId) {
            return $http.get(':api/product/list/'+productListId);
        }

        /**
         *
         * @param productListId
         * @return {HttpPromise}
         */
        function setProductList(productListId) {
            return $http.post(':api/product/list',productListId);
        }

        /**
         *
         * @param productListId
         * @return {HttpPromise}
         */
        function deleteProductList(productListId) {
            return $http.post(':api/product/list/'+productListId);
        }

        return LocalDataService;
    }]);
})();