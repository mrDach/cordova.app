/**
 * Created by honcharov_victor on 17.04.17.
 */
(function(){

    var app 		= angular.module('System.Services');

    /**
     *
     *	DataService
     *		- Checks each outbound router for :api and replaces it with the correct API path
     *
     *
     **/
    app.factory('DataService', [ '$http' , '$rootScope', '$cordovaNetwork', 'LocalDataService', 'RemoteDataService', '$cordovaDevice', function( $http, $rootScope, $cordovaNetwork, LocalDataService, RemoteDataService, $cordovaDevice ){

        var DataService = {
            get: _get,
            set: _set,
            delete: _delete,
            sending: sending,
            type: type,
            isOnline: isOnline,
            isOffline: !isOnline,
        };

        //Save the Object
        var obj   = this;

        var sending   = false;

        var type = 'none';

        var isOnline = true;

        //var isOffline = false;

        var StorageService = RemoteDataService;

        var platform = $cordovaDevice.getPlatform();

        if(platform != 'browser'){

            isOnline = false;

            isOffline = true;

            StorageService = LocalDataService;

            type = $cordovaNetwork.getNetwork();
            isOnline = $cordovaNetwork.isOnline();
            //isOffline = $cordovaNetwork.isOffline();

            if(type != 'none'){
                StorageService = RemoteDataService;
            }

            // listen for Online event
            $rootScope.$on('$cordovaNetwork:online', function(event, networkState){
                type = networkState;
                isOnline = true;
                //isOffline = false;
                StorageService = RemoteDataService;
            });

            // listen for Offline event
            $rootScope.$on('$cordovaNetwork:offline', function(event, networkState){
                type = networkState;
                isOnline = false;
                //isOffline = true;
                StorageService = LocalDataService;
            });

        }

        function _get(parameter,parameters) {
            sending = true;
            return StorageService['get'+parameter](parameters);
        }

        function _set(parameter,value,parameters) {
            return StorageService['set'+parameter](value,parameters);
        }

        function _delete(parameter,value,parameters) {
            return StorageService['delete'+parameter](value,parameters);
        }

        return DataService;
    }]);

})();