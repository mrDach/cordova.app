/**
 * Created by honcharov_victor on 17.04.17.
 */
(function(){

    var app 		= angular.module('System.Services');

    /**
     *
     *	DataService
     *		- Checks each outbound router for :api and replaces it with the correct API path
     *
     *
     **/
    app.factory('LocalDataService', [ '$http', function( $http ){

        var LocalDataService = {
            get: get,
            set: set,
        };

        //Save the Object
        var obj   = this;

        function get(parameter) {
            return parameter;
        }

        function set(parameter,value) {

        }

        return LocalDataService;
    }]);

})();