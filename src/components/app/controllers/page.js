(function(){

	var app = angular.module('System.Controllers');

	//The App Controller
	app.controller('PageController', [ '$state' , '$scope' , '$location' , '$rootScope', 'Config' , 'DataService' , function( $state , $scope , $location, $rootScope, Config, DataService){

		(function($){


			/**
			 *
			 *	Prevent Browser from Scrolling
			 *		- Prevents Scrolling elements from moving the browser when reaching the bottom of the scroll box
			 *
			 **/
			$('[data-scroll-box]').mousewheelStopPropagation();



			/**
			 *
			 *	Loads the Calendar
			 *
			 *
			 **/
			$('[data-calendar]').fullCalendar({

			});



			/**
			 *
			 * 	CLICK: [data-menu-control]
			 *		- Controls the Menu
			 *
			 *
			 **/
			$('[data-menu-control]').click(function(e){

				e.stopPropagation();

				$('#sidebar').toggleClass('open');

				$('[data-menu-control]').toggleClass('fa-bars fa-times');

			});





			/**
			 *
			 *	CLICK: #sidebar li
			 *		- Close the menu when clicking a link
			 *
			 *
			 **/
			$('#sidebar li').click(function(e){
				if( $(this).find('ul').length == 0 ){

					$('[data-menu-control]:visible').click();

				}
			});





			/**
			 *
			 *	CLICK: body
			 *		- When clicking the Body, if the target is not a part of the sidebar, close the sidebar
			 *
			 *
			 **/
			$('body').click(function(e){
				if( $( e.target ).closest('#sidebar').length == 0 && $('#sidebar').hasClass('open') ){

					$('[data-menu-control]:visible').click();

				}
			});




			/**
			 *
			 * 	CLICK: [data-menu-control]
			 *		- Controls the Menu
			 *
			 *
			 **/
			$('#header section .showbtn').click(function(e){

				$(this).parent().toggleClass('show');
				$('.showbtn').not(this).parent().removeClass('show');

			});




			/**
			 *
			 * 	ON FOCUS: body :input
			 *		- On Mobile Browsers, Hide the any error reporting while typing in an element
			 *
			 **/
			$('body').on('focus', ':input', function(){
				if( $(window).width() <= 500 ){
					$('.bs-callout-danger').addClass('hide');
				}
			});





			/**
			 *
			 * 	ON BLUR: body :input
			 *		- On Mobile Browsers, Show any error reporting message that were previously hidden
			 *
			 **/
			$('body').on('blur', ':input', function(){
				$('.bs-callout-danger').removeClass('hide');
			});

			/**
			 *
			 * 	When page is loaded turn blur off and hide load element
			 *
			 **/
			window.onload = function() {
				var div = document.querySelector('.loading_test');
				div && div.classList.remove('loading_test')
				var div = document.querySelector('.loading_symbol');
				div && div.classList.add('hide')
			};



			//APPEAR ANIMATIONS
// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
			(function($) {

				"use strict";

				$.fn.pluginAnimate = function(opt) {
					var elem = $(this);
					elem.addClass('animated unshown');

					var options = $.extend( {}, $.fn.pluginAnimate.defaults, opt );

					elem.appear(function() {
						var delay = ( elem.data('animation-delay') ? elem.data('animation-delay') : 1 );

						if( delay > 1 ) elem.css('animation-delay', delay + 'ms');
						elem.addClass( elem.data('animation-name') );

						setTimeout(function() {
							elem.addClass('shown').removeClass('unshown');
						}, delay);

					},{accX: options.accX, accY: options.accY});

					return this;
				};

				$.fn.pluginAnimate.defaults = {
					accX : 0,
					accY : -50
				};
			}(jQuery));

			//SCROLL TO TOP
			// =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
			var scroll = $('.scroll-to-top');

			// Scroll to top
			scroll.on('click',function () {
				$("html, body").animate({
					scrollTop: 0
				}, 600);
				return false;
			});

			// Show-Hide scrollToTop Button
			$(window).on('scroll',function () {
				if ($(this).scrollTop() > 100) {
					scroll.fadeIn();
				} else {
					scroll.fadeOut();
				}
			});

		})(jQuery);

        /**
		 * Returns the global Config object
         * @type {Config}
         */
        $scope.Config = Config;

        /**
		 * Returns the DataService object
         * @type {DataService}
         */
        $scope.DataService = DataService;

        /**
		 * Checks the Form for Errors Passed
         * ng-messages MUST set form.error[key] = Bool
         * I.E. ng-show="form.error.email = ( form.email.$dirty && form.email.$invalid )"
         * @param form {Object} The NG Form object to Compare
         * @return {boolean}
         */
        $scope.hasError = function (form) {
            if( form && form.error ){

                for( var key in form.error ){

                    if( form.error[ key ] ){

                        return true;
                    }

                }

            }

            return false;
        };

        /**
         *
         *   $scope.get
         *       -  Returns the Current Page Data
         *
         *   Params:
         * 		n/a
         *
         *	Returns:
         * 		1. The Current Page Data
         *
         **/
        $scope.get = function(){

            return $state.current;

        };

	}]);


	//Handles the Document Title
	app.run([ '$rootScope' , '$location' , function($rootScope , $location) {

		$rootScope.history = [];

	    $rootScope.$on('$stateChangeSuccess', function (event, current, previous) {

	    	$rootScope.history.push( $location.path() );

	    });

	}]);


})();