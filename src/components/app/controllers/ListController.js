/**
 * Created by honcharov_victor on 22.04.17.
 */
(function(){

    'use strict';

    var app = angular.module('System.Controllers');

    //The App Controller
    app.controller('ListController', ['$scope', 'DataService', '$location', function ($scope, DataService, $location) {

        $scope.page = {
            "data": [],
            "total": 0,
            "current": 1,
            "list": {
                "loading": true
            },
            "model":$scope.get().model
        };
        $scope.loader = [];

        $scope.load = function (page) {
            $scope.page.list.loading = true;
            DataService.get($scope.page.model+'List',{"limit":$location.search().limit,"page":page}).then(function (response) {

                $scope.page.list.loading = false;

                if (!response.data.result) {

                    $scope.errors = response.data.errors;

                } else {

                    $scope.page.data = response.data.data;
                    $scope.page.total = response.data.total;

                }

            });
        };

        $scope.delete = function (item) {
            $scope.page.list.loading = true;
            DataService.delete($scope.page.model,item.id).then(function (response) {
                $scope.page.list.loading = false;

                if (!response.data.result) {

                    $scope.errors = response.data.errors;

                } else {

                    $scope.load($location.search().page || 1);

                }

            });
        };

        //On "Click Grab"
        $scope.grab = function( source ){
            $scope.loader[source.id] = true;
            DataService.set('GrabSource', source.id).then(function (response) {
                if (!response.data.result) {

                    $scope.loader[source.id]    =  false;
                    $scope.errors 	= response.data.errors;

                } else {

                    $scope.errors 	= [];
                    $scope.loader[source.id]    =  false;

                }

            });
        };

        //Load the First Page
        $scope.load($location.search().page || 1);
        //set default dir-paginate variable for current page when fisrt loading
        //current-page -- can't use cause of double request while changing limit not on first page
        $scope.__default__currentPage = $location.search().page || 1;

    }]);

})();