/**
 * Created by honcharov_victor on 29.03.17.
 */
(function(){


    var app = angular.module('System.Views');

    app.config([ '$stateProvider' , '$urlRouterProvider' , function( $stateProvider , $urlRouterProvider ){

        $urlRouterProvider.when('', '/');

        $stateProvider.state({

            name: 			'dashboard',

            url: 			'/',

            params: 		{ squash:true },

            templateUrl: 	'components/dashboard/views/dashboard.html',

            controller: 	'DashboardController',

            breadcrumbs: 	[ { icon: 'home' , url:'/', name: 'Dashboard' } ]

        });

    }]);


})();