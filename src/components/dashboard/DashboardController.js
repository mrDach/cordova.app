/**
 * Created by honcharov_victor on 29.03.17.
 */
(function(){

    'use strict';

    var app = angular.module('System.Controllers');

    //The App Controller
    app.controller('DashboardController', [ '$scope' , 'DataService' ,'$http' ,  function( $scope, DataService, $http ){

        DataService.get('SourceList', {"limit":0}).then(function (response) {

            $scope.sources = response.data.data;

        });

        $http.get(':api/source/statistic').then(function (response) {

            $scope.statistic = response.data.data;

        });

        $scope.labels = ["January", "February", "March", "April", "May", "June", "July"];
        $scope.series = ['Privat24', 'Наличка'];
        $scope.data = [
            [11275, 8275, 15630, 21035, 9047, 18387, 27220],
            [2000, 1800, 1500, 1800, 2000, 1000, 3200]
        ];

    }]);

})();