/**
 * Created by honcharov_victor on 28.03.17.
 */
(function(){
    var app 		= angular.module('System.Directives');

    app.directive('siteHeader', header);
    header.$inject = ['$rootScope'];
    function header($rootScope){
        return {
            restrict: 'E',
            transclude: true,
            templateUrl: 'components/header/views/header.html',
            link : function (scope, element) {

                scope.toggleMainMenu  = toggleMainMenu;

                // NOTIFICACTION HEADERBOX
                // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
                function openItem(item) {
                    item.children('.dropdown-box').slideDown(400);
                }
                function closeItem(item) {
                    item.children('.dropdown-box').slideUp(200);
                }
                function closeSiblings(item) {
                    item.siblings('.notice.open').each(function () {
                        closeItem($(this));
                        $(this).removeClass('open');
                    });
                }
                $('#notice-headerbox .notice i').on('click', function (event) {

                    var item = $(this).parent();
                    item.toggleClass('open');

                    if (item.hasClass('open')) {
                        closeSiblings(item);
                        openItem(item)
                    } else {
                        closeItem(item)
                    }
                });

                //USER HEADERBOX DROPDOWN
                // =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
                $('#user-headerbox').on('click', function (event) {

                    var options = $(this).children('.user-options');
                    $(this).toggleClass('open');

                    if ($(this).hasClass('open')) {
                        options.slideDown(400);
                    } else {
                        options.slideUp(400);
                    }
                });

                /**
                 *
                 *   toggleMainMenu
                 *       - Toggle navigation menu (hide or show)
                 *
                 **/
                function toggleMainMenu() {
                    if ($rootScope.hideMainMenu == undefined || $rootScope.hideMainMenu == false) {
                        $rootScope.hideMainMenu = true;
                    } else if ($rootScope.hideMainMenu == true) {
                        $rootScope.hideMainMenu = false;
                    }
                }

            }

        }

    }
})();