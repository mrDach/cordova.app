<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        //Temporarily disable checks
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        //Truncate Table
        DB::table('users')->truncate();

        //Insert the Default Admin User
        DB::table('users')->insert(
            [ 'username' => 'admin' , 'firstname' => 'Admin' , 'lastname' => 'User' , 'password' => bcrypt('password'), 'email' => 'admin@admin.admin', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s') ]
        );

        //Insert the Default User
        DB::table('users')->insert(
            [ 'username' => 'test' , 'firstname' => 'test' , 'lastname' => 'test' , 'password' => bcrypt('Bestpassever'), 'email' => 'test@test.test', 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s') ]
        );

        //Reset Checks
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        
    }
}
