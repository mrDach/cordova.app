<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductsTable extends Migration {

	public function up()
	{
		Schema::create('products', function(Blueprint $table) {
			$table->increments('id');
            $table->string('name', 50)->index();
            $table->integer('cost')->unsigned();
            $table->integer('amount')->unsigned();
            $table->integer('product_list_id')->unsigned();
            $table->integer('product_category_id')->unsigned();
            $table->timestamps();
        });
	}

	public function down()
	{
		Schema::drop('products');
	}
}