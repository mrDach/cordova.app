<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductListsTable extends Migration {

	public function up()
	{
		Schema::create('product_lists', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->string('name');
			$table->datetime('date');
			$table->boolean('regular');
		});
	}

	public function down()
	{
		Schema::drop('product_lists');
	}
}