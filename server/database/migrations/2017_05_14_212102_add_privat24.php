<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPrivat24 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('privat24', function(Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('merchant_id');
            $table->string('merchant_pass');
            $table->string('card_number');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('privat24');
    }
}
