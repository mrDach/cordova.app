<?php
/**
 * Created by PhpStorm.
 * User: honcharov_victor
 * Date: 14.05.17
 * Time: 15:47
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Source extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    public $table = 'sources';
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $fillable = ['id', 'name', 'morph_type', 'morph_id'];

    public $appends = ['amount'];
    public $with = ['morph'];

    public $validator_rules = [
        'name' => 'required',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function morph(){
        return $this->morphTo();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function operations(){
        return $this->hasMany('App\Models\Operation');
    }

    public function getAmountAttribute(){
        return $this->operations()->sum('amount');
    }

}