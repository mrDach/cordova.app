<?php
/**
 * Created by PhpStorm.
 * User: honcharov_victor
 * Date: 15.05.17
 * Time: 0:25
 */

namespace app\Models;

use Illuminate\Database\Eloquent\Model;

class Privat24 extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    public $table = 'privat24';
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $fillable = ['id', 'merchant_id', 'merchant_pass', 'card_number'];

}