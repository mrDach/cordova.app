<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
	protected $table = 'products';

	public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'name', 'cost', 'amount', 'product_list_id', 'product_category_id'];

    public $validator_rules = [
        'name' => 'required',
        'product_list_id' => "required|integer|exists:product_lists,id",
        'product_category_id' => "required|integer|exists:product_categorys,id",
    ];

	public function productList()
	{
		return $this->belongsToMany('App\Models\ProductList');
	}

	public function category()
	{
		return $this->belongsTo('App\Models\ProductCategory','product_category_id');
	}

}