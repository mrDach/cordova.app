<?php
/**
 * Created by PhpStorm.
 * User: honcharov_victor
 * Date: 14.05.17
 * Time: 17:16
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Operation extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    public $table = 'operations';
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $fillable = ['id', 'source_id', 'product_list_id', 'amount', 'date'];
    public $with = ['source'];

    public $validator_rules = [
        'source_id' => "required|integer|exists:sources,id",
        'product_list_id' => "integer|exists:product_lists,id",
    ];

    public function source(){
        return $this->belongsTo('App\Models\Source');
    }

    public function product_list(){
        return $this->belongsTo('App\Models\ProductList','product_list_id','id');
    }

}