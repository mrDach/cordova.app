<?php
/**
 * Created by PhpStorm.
 * User: honcharov_victor
 * Date: 23.04.17
 * Time: 15:17
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    public $table = 'product_categorys';
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $fillable = ['id', 'name'];

    public $validator_rules = [
        'name' => 'required',
    ];

}