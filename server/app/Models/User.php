<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

use Bican\Roles\Traits\HasRoleAndPermission;
use Bican\Roles\Contracts\HasRoleAndPermission as HasRoleAndPermissionContract;

use App\Traits\HasStoreRelationship;
use App\Contracts\HasStoreRelationship as HasStoreRelationshipContract;
use App\Traits\Search as UserSearch;
use App\Contracts\Search as UserSearchContract;

use DB;
use Storage;

class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract, 
                                    HasStoreRelationshipContract,
                                    HasRoleAndPermissionContract,
                                    UserSearchContract
{
    use Authenticatable, Authorizable, CanResetPassword, HasStoreRelationship, HasRoleAndPermission, UserSearch {
        HasRoleAndPermission ::can insteadof Authorizable;
    }


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['firstname', 'lastname', 'gender', 'username', 'email', 'password', 'city', 'province', 'phone', 'avatar_id', 'facebook_id', 'twitter_id'];



    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['avatar'];


    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token', 'avatar_id'];



    /**
     * width for avatar.
     *
     * @var string
     */
    const WIDTH = 140;



    /**
     * height for avatar.
     *
     * @var string
     */
    const HEIGHT = 140;



    /**
    *
    *   readNotification
    *       - Sets the Notification as Read
    *
    *   Params:
    *       $notificationid:        (INT) The Notification ID to Assign as Read
    *
    *   Returns:
    *       n/a
    *
    **/
    public function readNotification( $notificationid ){
        $this->belongsToMany('App\Models\Notification')->updateExistingPivot( $notificationid , [
            'read' => 1
        ] );
    }



    /**
    *
    *   notifications
    *       - Loads the Belongs to Many Relationship Attachments
    *
    *   URL Params:
    *       n/a
    *
    *
    *   Returns (Object):
    *       1. The Notification to Create
    *
    **/
    public function notifications(){

        return $this->morphMany('App\Models\Notification', 'link');

    }



    /**
     *
     *   avatar
     *       - Loads Attachment
     *
     *   Params:
     *
     *
     *   Returns (Object):
     *       1. The attachment
     *
     **/
    public function avatar(){

        return $this->hasOne('App\Models\Attachment', 'id', 'avatar_id');

    }



    /**
     *
     *   getAvatarAttribute
     *       - get user avatar
     *
     *   Params:
     *
     *
     *   Returns (Object):
     *       1. The attachment
     *
     **/
    public function getAvatarAttribute(){

        $attachment = Attachment::find($this->avatar_id);
        if ($attachment != null) {
            $attachment->resize(User::WIDTH, User::HEIGHT, 'public');
        }
        return $attachment;

    }



    /**
     *
     *   setAvatarAttribute
     *       - delete exist avatar and sets new
     *
     *   Params:
     *      - avatar (Array) The User's Attachment for avatar
     *
     *   Returns:
     *       n/a
     *
     **/
    public function setAvatarAttribute($avatar){

        if (isset($this->attachment_id) && $this->attachment_id != $avatar['id']) {
            $this->deleteAvatar();
        }
        if(isset($avatar['id'])){
            $this->avatar_id = $avatar['id'];
        }else{
            $this->avatar_id = null;
        }

    }



    /**
     *
     *   delete
     *       - Delete an Attachment and files
     *
     *   Returns:
     *       n/a
     *
     **/
    public function delete(){

        $this->deleteAvatar();

        parent::delete();
    }



    /**
     *
     *   deleteAvatar
     *       - Delete avatar and resize copy
     *
     *   Returns:
     *       n/a
     *
     **/
    function deleteAvatar(){
        if($this->avatar_id != null) {
            $attachment = Attachment::find($this->avatar_id);
            Storage::disk($attachment->disk)->delete($attachment->id . '-' . User::WIDTH . 'x' . User::HEIGHT . $attachment->mime->extension);
            $attachment->delete();
        }
    }

}