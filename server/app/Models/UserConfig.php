<?php
/**
 * Created by PhpStorm.
 * User: honcharov_victor
 * Date: 17.04.17
 * Time: 22:28
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserConfig extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_config';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'name', 'value'];


}