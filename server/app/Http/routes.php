<?php

    //Patterns
    Route::pattern('page',				'[0-9]+');
    Route::pattern('limit',				'[0-9]{1,100}+');
    Route::pattern('id',				'[0-9]+');
    Route::pattern('path',				'.*?');
    Route::pattern('provider',			'.*?');

	/*
	|--------------------------------------------------------------------------
	| Browser Sniffing
	|--------------------------------------------------------------------------
	*/

	//Browser Update Page
	Route::get('/browser',function(){
		return view('browser');
	});


	Route::group([ 'middleware' => 'browser' ],function(){ 




		/*
		|--------------------------------------------------------------------------
		| Application Routes
		|--------------------------------------------------------------------------
		*/

		//Dashboard Page
		Route::get('/{all}',['as' => 'dashboard', 'uses' => 'DashboardController@index' ])->where([ 'all' => '(?!api)(?!auth)(?!password).*' ]);

		//Login
            Route::get(     'auth/login',                                               'Auth\AuthController@getLogin');
            Route::post(    'auth/login',                                               'Auth\AuthController@loginSocialite');
            Route::get(     'auth/logout',                                              'Auth\AuthController@getLogout');

		//Forgot Email
            Route::get(     'password/email',                                           'Auth\PasswordController@getEmail');
            Route::post(    'password/email',                                           'Auth\PasswordController@postEmail');

		//Reset Password
            Route::get(     'password/reset/{token}',                                   'Auth\PasswordController@getReset');
            Route::post(    'password/reset',                                           'Auth\PasswordController@postReset');

        //socialite
            Route::get(     'api/v1/register/{provider}', ['as' => 'socialite.auth',          'uses' => 'Api\v1\ProfileController@redirectToProvider'] );
            Route::get(     'api/v1/register/{provider}/callback',                            'Api\v1\ProfileController@handleProviderCallback' );





		/*
		|--------------------------------------------------------------------------
		| API Routes
		|--------------------------------------------------------------------------
		*/

        //API Version 1
        Route::group([ 'prefix' => '/api/v1' ], function(){

            //Page Setup - GET
                Route::get( 	'layout/{path}', 								            'Api\v1\LayoutController@getLayout' );

            //UserConfig
                Route::get( 	'user/config',					                            'Api\v1\UserConfigController@get' );
                Route::post( 	'user/config',					                            'Api\v1\UserConfigController@set' );

            //ProductCategory
                Route::get( 	'product/category/{id}',					                'Api\v1\ProductCategoryController@get' );
                Route::post( 	'product/category',					                        'Api\v1\ProductCategoryController@edit' );
                Route::get( 	'product/category/{limit?}/{page?}',					    'Api\v1\ProductCategoryController@getPaged' );
                Route::post( 	'product/category/{id}', 					                'Api\v1\ProductCategoryController@delete' );

            //ProductList
                Route::get( 	'product/list/{id}',					                    'Api\v1\ProductListController@get' );
                Route::post( 	'product/list',					                            'Api\v1\ProductListController@edit' );
                Route::get( 	'product/list/{limit?}/{page?}',					        'Api\v1\ProductListController@getPaged' );
                Route::post( 	'product/list/{id}', 					                    'Api\v1\ProductListController@delete' );

            //Source
                Route::get( 	'source/{id}',					                            'Api\v1\SourceController@get' );
                Route::post( 	'source',					                                'Api\v1\SourceController@edit' );
                Route::get( 	'source/{limit?}/{page?}',					                'Api\v1\SourceController@getPaged' );
                Route::post( 	'source/{id}', 					                            'Api\v1\SourceController@delete' );
                Route::post( 	'source/{id}/grab', 					                    'Api\v1\SourceController@grab' );
                Route::get( 	'source/{id}/grab', 					                    'Api\v1\SourceController@grab' );
                Route::get( 	'source/statistic', 					                    'Api\v1\SourceController@statistic' );

            //Operation
                Route::get( 	'operation/{id}',					                        'Api\v1\OperationController@get' );
                Route::post( 	'operation',					                            'Api\v1\OperationController@edit' );
                Route::get( 	'operation/{limit?}/{page?}',					            'Api\v1\OperationController@getPaged' );
                Route::post( 	'operation/{id}', 					                        'Api\v1\OperationController@delete' );

        });

        //API Version 1
        Route::group([ 'prefix' => '/api/v1' , 'middleware' => 'auth' ], function(){

            //General Access - Users
                Route::get( 	'user/{userid}', 											'Api\v1\UserController@getUser' );
                Route::get( 	'user/roles/{userid?}', 									'Api\v1\UserController@getRoles' );
                Route::get( 	'user/stores/{userid?}', 									'Api\v1\UserController@getStores' );
                Route::get( 	'user/permissions/custom/{userid?}', 						'Api\v1\UserController@getCustomPermissions' );

            //General Access - Profile
                Route::post( 	'profile', 													'Api\v1\ProfileController@editProfile' );

			//Users
				Route::get( 	'users/{limit?}/{page?}', 			                        'Api\v1\UserController@getAllUsers' );
				Route::put( 	'user', 							                        'Api\v1\UserController@addUser' );
				Route::post( 	'user', 					                                'Api\v1\UserController@editUser' );
                Route::delete( 	'user/{userid}', 					                        'Api\v1\UserController@deleteUser' );
                Route::post( 	'user/avatar', 					                            'Api\v1\UserController@attach' );
                Route::get( 	'user/search', 					                            'Api\v1\UserController@searchUser' );

        });


	});