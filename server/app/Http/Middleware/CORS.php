<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Response;

class CORS
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $headers = [
            'Access-Control-Allow-Origin' => '*',
            'Access-Control-Allow-Methods'=> '*',
            'Access-Control-Allow-Headers'=> 'Access-Control-Allow-Headers, Origin, Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers'
        ];

        if($request->getMethod() == "OPTIONS") {

            $response = new Response();
            foreach($headers as $key => $value)
                $response->headers->set($key, $value);

            return $response;
        }

        $response = $next($request);

        foreach($headers as $key => $value)
            $response->headers->set($key, $value);

        return $response;
    }
}
