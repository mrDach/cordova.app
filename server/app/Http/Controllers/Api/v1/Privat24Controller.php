<?php
/**
 * Created by PhpStorm.
 * User: honcharov_victor
 * Date: 22.04.17
 * Time: 17:36
 */

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Api\v1\UserConfigController;

class Privat24Controller
{

    private $merchant_id;
    private $merchant_pass;
    private $card_number;

    function __construct($attributes = []) {

        $attributes = UserConfigController::get()['data'];
        $this->merchant_id = $attributes['merchant_id'];
        $this->merchant_pass = $attributes['merchant_pass'];
        $this->card_number = $attributes['card_number'];

    }

    public function get($source_id, $start, $end){

        /*$start = '1.01.2017';
        $end = '18.02.2017';*/
        $xml = $this->xml_for_info($this->card_number, $start, $end);
        $resp = $this->curl_for_info($xml);

        $data=unserialize(serialize(json_decode(json_encode((array) simplexml_load_string($resp)), 1)));

        $now = \Carbon\Carbon::now();
        $result = [];
        foreach ($data['data']['info']['statements']['statement'] as $key => $datum){
            $item = $datum['@attributes'];
            $result[] = [
                'date' => $item['trandate'].' '.$item['trantime'],
                'amount' => floatval($item['cardamount']),
                'source_id' => $source_id,
                'created_at' => $now,
                'updated_at' => $now,
            ];
        }

        return $result;

    }

    /**
     * @param $cardnum
     * @param $start
     * @param $end
     * @return bool|\SimpleXMLElement
     */
    public function xml_for_info($cardnum, $start, $end){

        $data = array(
            'data'=>array(
                'oper'=>'cmt',
                'wait'=>100,
                'test'=>1,
                'payment'=>array(
                    '_attr'=>array(
                        'id'=>''
                    ),
                    'prop'=>array(
                        array(
                            '_attr'=>array(
                                'name'=>'sd',
                                'value'=>$start,
                            ),
                        ),
                        array(
                            '_attr'=>array(
                                'name'=>'ed',
                                'value'=>$end,
                            ),
                        ),
                        array(
                            '_attr'=>array(
                                'name'=>'card',
                                'value'=>$cardnum,
                            ),
                        )
                    ),
                ),
            ),
        );

        $data_xml = array_to_xml($data);

        $data_xml_string = '';

        foreach ($data_xml->children() as $second_gen) {
            $data_xml_string .= $second_gen->asXML();
        }

        $merchant_signature=sha1(md5($data_xml_string.$this->merchant_pass));

        $array=array(
            'request' => array(
                '_attr'=>array(
                    'version'=>'1.0'
                ),
                'merchant'=>array(
                    'id'=>$this->merchant_id,
                    'signature'=>$merchant_signature,
                ),
                'data'=>$data['data'],
            )
        );

        $xml = array_to_xml($array);
        return $xml;

    }

    /**
     * @param \SimpleXMLElement $xml
     * @return mixed
     */
    public function curl_for_info(\SimpleXMLElement $xml){

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://api.privatbank.ua/p24api/rest_fiz');

        curl_setopt($ch, CURLOPT_POST, 1);

        curl_setopt ($ch, CURLOPT_USERAGENT, "Mozilla/5.0");

        $headers = array
        (
            'Accept: */*',
            'Cache-Control: no-cache',
            'Content-Length: '.strlen($xml),
            'Content-Length: 0',
            'Content-Type: application/xml'
        );

        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml->asXML());

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 300);

        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
    }
}