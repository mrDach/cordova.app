<?php
/**
 * Created by PhpStorm.
 * User: honcharov_victor
 * Date: 15.05.17
 * Time: 6:24
 */

namespace app\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\ProductList;
use DB;
use Input;
use Validator;

class ProductListController extends Controller
{
    public function __construct()
    {
        $this->model = new ProductList();
    }

    public function get($id, array $with = [])
    {
        return parent::get($id, ['products.category']);
    }

    /**
     * edit Model
     * @return array
     */
    public function edit()
    {
        $data = call_user_func_array(['Input', 'only'], array_diff(
            array_merge($this->model->getFillable(),$this->model->getAttributes()), $this->model->getHidden()
        ));

        $validator_rules = $this->model->validator_rules;

        if (isset($data['id'])) {
            $validator_rules = [
                'id' => "required|integer|exists:" . $this->model->table . ",id",
            ];
        }

        $validator = Validator::make($data, $validator_rules);

        if ($validator->fails()) {

            //Return Failure
            return ['result' => 0, 'errors' => $validator->errors()->all()];

        } else {

            DB::beginTransaction();
            try {

                //$this->model->updateOrCreate($data);

                if (isset($data['id'])) {
                    //Get the Model
                    $model = $this->model->find($data['id']);
                    $model->fill($data);
                    $model->save();

                } else {
                    $model = $this->model->create($data);
                }

                DB::commit();

                DB::beginTransaction();

                $products = Input::only('products')['products'];

                foreach ($products as $key => $product){
                    $products[$key]['product_list_id'] = $model->id;
                    $products[$key]['product_category_id'] = $product['category']['id'];
                    unset($products[$key]['category']);
                }

                Product::insert($products);

                DB::commit();

                //Return Success
                return ['result' => 1];

            } catch (Exception $e) {

                DB::rollback();
                //Return Failure
                return ['result' => 0, 'errors' => [$e->getMessage()]];

            }

        }

    }

}