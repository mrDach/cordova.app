<?php
/**
 * Created by PhpStorm.
 * User: honcharov_victor
 * Date: 17.04.17
 * Time: 22:28
 */

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Models\UserConfig;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class UserConfigController
{
    static public function get(){

        //Get the UserConfig
        if ($config = UserConfig::whereUserId(1)->get(['name','value']) ) {

            $data = [];
            foreach ($config as $item){
                $data[$item['name']]=$item['value'];
            }

            //Return Result
            return [
                'data' => $data,
                'result' => 1

            ];

        }

        //Return Failed
        return ['result' => 0, 'errors' => ['That Config Doesn\'t exist']];

    }

    static public function set(){

        $data = Input::get();

        DB::beginTransaction();
        try {

            foreach ( $data as $key => $value ) {

                if( $config = UserConfig::whereUserId(1)->whereName($key)->first() ){

                    $config->value = $value;
                    $config->save();

                }else{

                    UserConfig::create([
                        'user_id'   => 1,
                        'name'      => $key,
                        'value'     => $value,
                    ]);

                }

            }

            DB::commit();
            return [ 'result' => 1 ];

        } catch (\Exception $e) {

            DB::rollback();
            //Return Failure
            return ['result' => 0, 'errors' => [$e->getMessage()]];

        }

    }
}