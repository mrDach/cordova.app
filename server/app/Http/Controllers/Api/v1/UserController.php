<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use Illuminate\Support\Str;
use Bican\Roles\Models\Role;
use Bican\Roles\Models\Permission;

use App\Models\User;
use App\Models\Attachment;

use App\Services\CommonService;

use Auth;
use Input;
use Validator;
use Hash;
use DB;
use Mail;

class UserController extends Controller {



    /**
    *   UNUSED
    *   getStatus
    *       - Checks if the Current User is Logged in
    *
    *   Params:
    *       n/a
    *
    *   Returns (JSON):
    *       1. The User Looked up (If $userid was passed)
    *       2. The Current User Session Data (If $userid was not passed)
    *       3. Null
    *
    **/
    public function getStatus(){    

        return [ 'result' => \Auth::check() ];

    }



    /**
    *
    *   getUser
    *       - Loads the User
    *
    *   Params:
    *       - $userid:      The UserID to Lookup
    *
    *   Returns (JSON):
    *       1. The User Looked up (If $userid was passed)
    *       2. The Current User Session Data (If $userid was not passed)
    *       3. Null
    *
    **/
    static public function getUser( $userid ){

        if( $user = User::where('id' , '=', $userid )->with('roles')->first() ){

            //Return the Data
            return [
                'result' => 1,
                'data'   => $user,
            ];

        }else{

            //Return Failure
            return [ 'result' => 0 , 'errors' => [ 'That User doesn\'t exist' ] ];

        }

    }
    


    /**
    *
    *   getAllUsers
    *       - Loads All of the Users
    *
    *   URL Params:
    * 		- limit: 	 The Page Limit (Default: 15)
    *       - page:      Pages to Load (Default: null)
    *
    *
    *   Returns (JSON):
    *       1. The Total Users
    *       2. A List of Users Paginated
    *
    **/
    public function getAllUsers( $limit = 15 , $page = null ){

        if( $page ) {

            $query  = User::take( $limit )->skip( ( ( $page - 1 ) * $limit ) );

            $total  = User::count();
            $data   = $query->get();

            //Return the Data
            return [
                'total'  => $total ,
                'data'   => $data,
                'result' => 1

            ];

        } elseif($page == null) {

            //Get the All User
            $data = User::all([ 'id', 'firstname', 'lastname' ]);

            //Return Result
            return [
                'data'   => $data,
                'result' => 1
            ];

        } else{

            //Return Failure
            return [ 'result' => 0 , 'errors' => [ 'Users doesn\'t exist' ] ];

        }

    }   



    /**
    *
    *   findUser
    *       - Loads the User based on the passed criteria ( username or email )
    *
    *   Params ($_GET):
    *       - email:            (String) The Email Address
    *       - username:         (String) The Username
    *       - userid:           (INT) Exclude the User ID from the Search
    *
    *   Returns (JSON):
    *       Returns the User data or null
    *
    **/
    static public function findUser($username = '', $email = ''){

        if ($email == '') {
            $email = Input::get('email', null);
        }

        if ($username == '') {
            $username = Input::get('username', null);
        }

        $userid     = Input::get( 'userid' , null );

        if( $email || $username ){

            $query = User::query();

            if( $email    ) $query->where( 'email'    , '=' , $email );

            if( $username ) $query->where( 'username' , '=' , $username );

            if( $userid   ) $query->where( 'id'       , '!=', $userid );

            //return $query->first();
            return [
                'data'   => $query->first(),
                'result' => 1
            ];

        }

        //Return Failure
        return [ 'result' => 0 , 'errors' => [ 'Either email or username must be passed as a GET variable' ] ];

    }



    /**
    *
    *   getRoles
    *       - Loads the User Roles
    *
    *   Params:
    *       - $userid:      The UserID to Lookup (Default: NULL)
    *
    *   Returns (JSON):
    *       1. Returns all of the Users Assigned Roels
    *
    **/
    public function getRoles( $userid = null ){

        //Return the Roles

        if( $user = User::find( ( $userid ? $userid : Auth::user()->id ) )->roles()->get() ){

            //Return the Data
            return [
                'result' => 1,
                'data'   => $user,
            ];

        }else{

            //Return Failure
            return [ 'result' => 0 , 'errors' => [ 'User roles doesn\'t exist' ] ];

        }
        
    }



    /**
    *
    *   getPermissions
    *       - Loads the User Permissions
    *
    *   Params:
    *       - $userid:      The UserID to Lookup (Default: NULL)
    *
    *   Returns (JSON):
    *       1. Returns all of the Users Assigned Roels
    *
    **/
    public function getPermissions( $userid = null ){

        //Return the Permissions
        if (Auth::check()) {

            $data = User::find(($userid ? $userid : Auth::user()->id))->getPermissions();

            if($data){

                return [
                    'result' => 1,
                    'data'   => $data,
                ];

            }else{

                //Return Failure
                return [ 'result' => 0 , 'errors' => [ 'User roles doesn\'t exist' ] ];

            }

        } else {

            //Return Failure
            return [
                'data'   => [],
                'result' => 0
            ];

        }

    }



    /**
    *
    *   getCustomPermissions
    *       - Loads the User's Custom Permissions (Not Associated to the Role)
    *
    *   Params:
    *       - $userid:      The UserID to Lookup (Default: NULL)
    *
    *   Returns (JSON):
    *       1. Returns all of the Users Assigned Roels
    *
    **/
    public function getCustomPermissions( $userid = null ){

        $data['id']     = $userid;

        $validator      = Validator::make( $data, [

            'id'        => 'sometimes|exists:users,id'

        ]);
        if( $validator->fails() ){

            //Return Failure
            return [ 'result' => 0 , 'errors' => $validator->errors()->all() ];

        }else {
            //Return the Permissions
            if ($data = User::find(($data['id'] ? $data['id'] : Auth::user()->id))->userPermissions()->get()) {

                return [
                    'result' => 1,
                    'data'   => $data,
                ];

            } else {

                //Return Failure
                return ['result' => 0, 'errors' => ['An Unknown Error Occured']];
            }
        }
    }



    /**
    *
    *   addUser
    *       - Create a New User
    *
    *   Params ($_PUT):
    *       - firstname:                (String) The User Firstname
    *       - lastname:                 (String) The User Lastname
    *       - username:                 (String) The User's Username
    *       - password:                 (String) The User's Password
    *       - password_confirmation     (String) A Password Confirmation
    *       - city:                     (String) The User's City
    *       - province:                 (String) The User's Province
    *       - phone:                    (String) The User's Phone
    *       - roles:                    (Array) The User's Roles
    *       - locations:                (Array) The Locations the User is added to
    *       - gender:                   (String) The User's Gender
    *       - avatar:                   (Array) The User's Attachment for avatar
    *
    *    Returns (Array):
    *        1. (Bool) Returns True / False
    *
    **/
    static public function addUser($user = []){

        $data       = array_merge(Input::all(),$user);
        $validator  = Validator::make( $data, [
            'firstname'                 => 'required',
            'lastname'                  => 'required',
            'username'                  => 'unique:users,username|min:3|max:50',
            'email'                     => 'required|unique:users,email',
            'password'                  => 'confirmed|min:6|regex:/^(?=.*[a-z])(?=.*[A-Z]).*$/',
            'gender'                    => 'sometimes|in:f,m',
            'city'                      =>  '',
            'province'                  =>  '',
            'phone'                     =>  '',
            'permissions.*.id'          =>  'sometimes|exists:permissions,id',
            'roles.*.id'                =>  'sometimes|exists:roles,id',
            'avatar.id'                 =>  'sometimes|exists:attachments,id',
        ]);

        if( $validator->fails() ){

            //Validation Failed
            return [ 'result' => 0 , 'errors' => $validator->errors()->all() ];

        }else{

            try {

                //Create the User
                $user = User::create(array_merge($data,[
                    'password'   => Hash::make( $data['password'] ),
                    'slug'       => Str::slug( $data['firstname'] . ' ' . $data['lastname'] . ' ' . $data['username'] ),
                    'username'   => ( !empty( $data['username'] ) ? $data['username'] : null ),
                    'avatar_id'   => ( !empty( $data['avatar'] ) ? $data['avatar']['id'] : null )
                ]));

                //Attach the Roles
                if( !empty( $data['roles'] ) ){

                    $role = Role::find( $data['roles'] );
                     
                    $user->attachRole( $role );

                }

                //Attach the Permissions
                if( !empty( $data['permissions'] ) ){

                    $permissions = Permission::find( $data['permissions'] );

                    foreach( $permissions as $permission ){

                        $user->attachPermission( $permission );

                    }

                }

                //Send the Notification
                $user->notifications()->create([

                    'icon'      => 'user',
                    'type'      => 'New User',
                    'details'   => $user->firstname . ' ' . $user->lastname ,
                    'url'       => '/admin/users/edit/' . $user->id
                
                ])->send([            
                
                    'permissions'   => [ Permission::where('slug' , 'users.edit' )->first()->id ],
                    'exclude'       => [ $user->id ]
                
                ]);

                //Add the Search Criteria
                $user->search([
                
                    'title'         => $user->firstname.' '.$user->lastname,
                    'query'         => implode(' ',[
                        $user->username,
                        $user->email,
                        $user->city,
                        $user->province,
                        $user->phone
                    ]),
                    'url'           => '/admin/users/edit/' . $user->id
                
                ])->assign([
                
                    'permissions'   => [ Permission::where('slug' , 'users.edit' )->first()->id ]
                
                ]);

                /**
                * Get all users including administrators
                */
                $Recipients = CommonService::getUsersForSendingMails(array($user));

                //Send the Email
                Mail::send([ 'html' => 'emails.registration'] , $data , function($message) use ($Recipients){

                    foreach($Recipients as $user){
                    //Add the User
                    $message->to( $user->email , $user->firstname . ' ' . $user->lastname );
                    }

                    //Set Subject
                    $message->subject( 'Welcome to ' . env('COMPANY') );

                });

                //Return Success
                return [ 'result' => 1 ];
    
            } catch(Exception $e ){

                //Return Failure
                return [ 'result' => 0 , 'errors' => [ 'An Unknown Error Occured' ] ];

            }

        }

    } 



    /**
    *
    *   editUser
    *       - Update an Existing User
    *
    *   Params (URL):
    *       - userid:                   (String) The User ID
    *
    *   Params ($_POST):
    *       - firstname:                (String) The User Firstname
    *       - lastname:                 (String) The User Lastname
    *       - username:                 (String) The User's Username
    *       - password:                 (String) The User's Password
    *       - password_confirmation     (String) A Password Confirmation
    *       - city:                     (String) The User's City
    *       - province:                 (String) The User's Province
    *       - phone:                    (String) The User's Phone
    *       - roles:                    (Array) The User's Roles
    *       - locations:                (Array) The Locations the User is added to
    *       - avatar:                   (Array) The User's Attachment for avatar
    *
    *    Returns (Array):
    *        1. (Bool) Returns True / False
    *
    **/
    static public function editUser($user = []){

        $data       = array_merge(Input::all(),$user);

        $validator  = Validator::make( $data, [
            'id'                        => 'required|integer|exists:users',
            'firstname'                 => 'required',
            'lastname'                  => 'required',
            'username'                  => 'min:3|max:50',
            'email'                     => 'required|unique:users,email,' . $data['id'],
            'password'                  => 'confirmed|min:6|regex:/^(?=.*[a-z])(?=.*[A-Z]).*$/',
            'gender.id'                 => 'sometimes|in:f,m',
            'city'                      =>  '',
            'province'                  =>  '',
            'phone'                     =>  '',
            'permissions.*.id'          =>  'sometimes|exists:permissions,id',
            'roles.*.id'                =>  'sometimes|exists:roles,id',
            'avatar.id'                 =>  'sometimes|exists:attachments,id',
        ]);

        if( $validator->fails() ){

            //Validation Failed
            return [ 'result' => 0 , 'errors' => $validator->errors()->all() ];

        }else{

            //Get the User
            $user = User::find( $data['id'] );

            try {

                //Setup the User
                if( !empty( $data['password'] ) ){

                    $user->password = Hash::make( $data['password'] );

                }

                $user->firstname        = $data['firstname'];
                $user->lastname         = $data['lastname'];
                $user->username         = ( !empty( $data['username'] ) ? $data['username'] : null );
                $user->slug             = Str::slug( $data['firstname'] . ' ' . $data['lastname'] . ' ' . $data['username'] );
                $user->email            = $data['email'];
                $user->gender           = ( isset( $data['gender']['id'] ) ? $data['gender']['id'] : null );
                $user->city             = $data['city'];
                $user->province         = $data['province'];
                $user->phone            = $data['phone'];
                $user->avatar           = $data['avatar'];

                $user->save();

                if(isset($data['roles'])){

                    //Setup the Roles
                    $user->detachAllRoles();

                    if( !empty( $data['roles'] ) ){

                        $user->attachRole( Role::find( $data['roles'] ) );

                    }

                }

                if(isset($data['permissions'])) {

                    //Setup the Permissions
                    $user->detachAllPermissions();

                    if (!empty($data['permissions'])) {

                        $permissions = Permission::find($data['permissions']);

                        foreach ($permissions as $permission) {

                            $user->attachPermission($permission);

                        }

                    }
                }

                //Send the Notification
                $user->notifications()->create([

                    'icon'      => 'user',
                    'type'      => 'User Updated' ,
                    'details'   =>  $user->firstname . ' ' . $user->lastname ,
                    'url'       => '/admin/users/edit/' . $user->id
                
                ])->send([            
                
                    'permissions'   => [ Permission::where('slug' , 'users.edit' )->first()->id ]
                
                ]);

                //Update the Search Criteria
                $user->search([
                
                    'title' => $user->firstname.' '.$user->lastname,
                    'query' => implode(' ',[
                        $user->username,
                        $user->email,
                        $user->city,
                        $user->province,
                        $user->phone
                    ])
                
                ]);

                //Return Success
                return [ 'result' => 1 ];

            } catch( Exception $e ){

                //Return Failure
                return [ 'result' => 0 , 'errors' => [ 'An Unknown Error Occured' ] ];

            }

        }

    }     



    /**
    *
    *   deleteUser
    *       - Delete an Existing User
    *
    *   Params (URL):
    *       - userid:                   (String) The User ID
    *
    *   Returns (Array):
    *       1. (Bool) Returns True / False
    *
    **/
    public function deleteUser( $userid ){

        $data['id']     = $userid;

        $validator      = Validator::make( $data, [

            'id'        => 'required|exists:users,id'

        ]);
        if( $validator->fails() ){

            //Return Failure
            return [ 'result' => 0 , 'errors' => $validator->errors()->all() ];

        }else {

            if ($user = User::find($data['id'])) {

                //Delete the Search
                $user->search()->delete();

                //Clear the Notifications
                $user->notifications()->delete();

                //Send the Notification
                $user->notifications()->create([

                    'icon'      => 'user',
                    'type'      => 'User Deleted',
                    'details'   => $user->firstname . ' ' . $user->lastname,
                    'url'       => null

                ])->send([

                    'permissions' => [Permission::where('slug', 'users')->first()->id]

                ]);

                //Delete the User
                $user->delete();

                //Return Success
                return ['result' => 1];

            } else {

                //Return Failure
                return ['result' => 0, 'errors' => ['That User doesn\'t exist']];

            }
        }

    }



    /**
     *
     *   attach
     *       - Attaches an avatar, for resizing image
     *
     *   Request Params:
     *       file:       (FILE) The File to Attach
     *       directory:  (String) Path where save the attachment
     *
     *
     *   Returns (JSON):
     *       1. The Attachment
     *
     **/
    public function attach(){

        $data = Input::all();

        $validator      = Validator::make( $data, [
            'file'      => 'required',
            'directory' => ''
        ]);

        if( $validator->fails() ){

            //Return Failure
            return [ 'result' => 0 , 'errors' => $validator->errors()->all() ];

        }else {

            if (isset($data['directory'])) {

                $attachment = Attachment::store($data['file'], $data['directory']);
                $attachment->resize(User::WIDTH, User::HEIGHT, $data['directory']);

            } else {

                $attachment = Attachment::store($data['file']);
                $attachment->resize(User::WIDTH, User::HEIGHT, 'public');

            }

            return ['result' => 1, 'attachment' => $attachment];
        }

    }



    /**
     *
     *   searchUser
     *       - Search the User based on the keyword
     *
     *   Params ($_GET):
     *       - search:             (String) Keyword
     *
     *   Returns (JSON):
     *       Returns the User's data or null
     *
     **/
    public function searchUser(){

        $search = Input::get('search', null);

        if( $search ){

            $query = User::query()
                ->where( function ( $query ) use ($search) {
                    $query->orWhere('email', 'LIKE', '%' . $search . '%')
                        ->orwhere('firstname', 'like', '%' . $search . '%')
                        ->orWhere('lastname', 'LIKE','%' .  $search . '%')
                        ->orWhere('username', 'LIKE', '%' . $search . '%');
                })
                ->whereNotIn('id', [Auth::user()->id]);


            return [
                'data'   => $query->take(20)->get(),
                'result' => 1
            ];

        }

        //Return Failure
        return [ 'result' => 0 , 'errors' => [ 'no User found' ] ];

    }


}
