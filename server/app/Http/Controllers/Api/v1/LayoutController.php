<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use View;
use Response;

class LayoutController extends Controller {


    /**
    *
    *   getLayout
    *       - Returns the Layout
    *
    *   Params:
    *       - path:       (String) The Path to Layout
    *
    *   Returns (HTML):
    *       1. Returns the Layout
    *
    **/
    public function getLayout( $path ){

        if( View::exists( $path ) ){

            return View::make( $path );

        }

        return Response::make( array( 'result' => 0 , 'error' => 'Invalid Layout File' , 'code' => 'invalid-layout-file' ) , 404 );
    
    }

    
}
