<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\User;
use DB;
use Input;
use Validator;
use Config;
use App\Models\FileLoader;

class SettingsController extends Controller {



    /**
     *
     *   getSettings
     *       - Loads All settings
     *
     *   Returns (JSON):
     *       1. The Total settings
     *       2. Null
     *
     **/
    public function getSettings(){

        if ( $query  = Config::get('options') ) {

            //Return the Data
            return [
                'data'   => $query,
                'result' => 1

            ];

        } else{

            //Return Failure
            return [ 'result' => 0 , 'errors' => [ 'Settings doesn\'t exists' ] ];

        }

    }



    /**
     *
     *   editSettings
     *       - Update All settings
     *
     *   Params ($_POST):
     *       - time:                (String) time of setting
     *       - toggle:                 (String) toggle of setting
     *
     *    Returns (Array):
     *        1. (Bool) Returns True / False
     *
     **/
    public function editSettings(){

        $data       = array_merge(Input::all());

        $validator  = Validator::make( $data, [
            'feedInterval'                      => 'required',
            'messageInterval'                   => 'required',
            'facebook.client_id'                => 'required',
            'facebook.client_secret'            => 'required',
            'facebook.redirect'                 => 'required',
            'twitter.client_id'                 => 'required',
            'twitter.client_secret'             => 'required',
            'twitter.redirect'                  => 'required',
        ]);

        if( $validator->fails() ){

            //Validation Failed
            return [ 'result' => 0 , 'errors' => $validator->errors()->all() ];

        }else{

            try {

                Config::write('options', [
                    'feedInterval'          => $data['feedInterval'],
                    'messageInterval'       => $data['messageInterval'],
                    'facebook'              => [
                        'client_id'         => $data['facebook']['client_id'],
                        'client_secret'     => $data['facebook']['client_secret'],
                        'redirect'          => $data['facebook']['redirect']
                    ],
                    'twitter'               => [
                        'client_id'         => $data['twitter']['client_id'],
                        'client_secret'     => $data['twitter']['client_secret'],
                        'redirect'          => $data['twitter']['redirect']
                    ]
                ]);

                //Return Success
                return [ 'result' => 1 ];

            } catch( Exception $e ){

                //Return Failure
                return [ 'result' => 0 , 'errors' => [ 'An Unknown Error Occured' ] ];

            }

        }

    }

}
