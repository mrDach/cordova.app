<?php
/**
 * Created by PhpStorm.
 * User: honcharov_victor
 * Date: 23.04.17
 * Time: 15:17
 */

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Models\ProductCategory;

class ProductCategoryController extends Controller
{

    public function __construct()
    {
        $this->model = new ProductCategory();
    }

}