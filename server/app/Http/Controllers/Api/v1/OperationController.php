<?php
/**
 * Created by PhpStorm.
 * User: honcharov_victor
 * Date: 14.05.17
 * Time: 17:35
 */

namespace app\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Models\Operation;

use Illuminate\Database\Eloquent\Model;
use DB;
use Input;
use Validator;


class OperationController extends Controller
{

    public function __construct()
    {
        $this->model = new Operation();
    }

    public function get($id, array $with = [])
    {
        return parent::get($id, ['product_list']);
    }

    /**
     * edit Model
     * @return array
     */
    public function edit()
    {

        $data = call_user_func_array(['Input', 'only'], array_diff(
            array_merge($this->model->getFillable(),$this->model->getAttributes()), $this->model->getHidden()
        ));

        $validator_rules = $this->model->validator_rules;

        if (isset($data['id'])) {
            $validator_rules = [
                'id' => "required|integer|exists:" . $this->model->table . ",id",
            ];
        }

        if(isset(Input::only('source')['source'])){
            $data['source_id'] = Input::only('source')['source']['id'];
        }
        if(isset(Input::only('product_list')['product_list'])){
            $data['product_list_id'] = Input::only('product_list')['product_list']['id'];
        }

        $validator = Validator::make($data, $validator_rules);

        if ($validator->fails()) {

            //Return Failure
            return ['result' => 0, 'errors' => $validator->errors()->all()];

        } else {

            DB::beginTransaction();
            try {

                //$this->model->updateOrCreate($data);

                if (isset($data['id'])) {
                    //Get the Model
                    $category = $this->model->find($data['id']);
                    $category->fill($data);
                    $category->save();

                } else {
                    $this->model->create($data);
                }

                DB::commit();
                //Return Success
                return ['result' => 1];

            } catch (Exception $e) {

                DB::rollback();
                //Return Failure
                return ['result' => 0, 'errors' => [$e->getMessage()]];

            }

        }

    }

}