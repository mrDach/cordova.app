<?php
/**
 * Created by PhpStorm.
 * User: honcharov_victor
 * Date: 14.05.17
 * Time: 16:02
 */

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Models\Operation;
use App\Models\Source;
use DB;
use Input;
use Validator;

class SourceController extends Controller
{

    public function __construct()
    {
        $this->model = new Source();
    }

    public function grab($id)
    {
        $validator = Validator::make(
            [
                'id' => $id
            ],
            [
                'id' => "required|integer|exists:" . $this->model->table . ",id",
            ]
        );

        if ($validator->fails()) {

            //Return Failure
            return ['result' => 0, 'errors' => $validator->errors()->all()];

        } else {

            DB::beginTransaction();
            try {

                $source = $this->model->with(['operations'=>function($query){
                    $query->orderBy('date', 'desc');
                    $query->limit(1);
                }])->find($id);
                if($source->morph_type == 'privat'){
                    $privat = new Privat24Controller($source->morph);
                }

                $end = \Carbon\Carbon::now();
                $start = \Carbon\Carbon::now()->subMonth();
                if(count($source->operations)>0){
                    $start = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $source->operations[0]->date);
                    $test = $end->diffInDays($start);
                    if($end->diffInDays($start)<=1){
                        return ['result' => 1];
                    }
                }
                //$privat->get($source->id,$start->format('d.m.Y'),$end->format('d.m.Y'));
                Operation::insert($privat->get($source->id,$start->format('d.m.Y'),$end->format('d.m.Y')));

                DB::commit();
                //Return Success
                return ['result' => 1];

            } catch (Exception $e) {

                DB::rollback();
                //Return Failure
                return ['result' => 0, 'errors' => [$e->getMessage()]];

            }

        }
    }

    public function statistic(){
        $operations =  Operation::with('source')->orderBy('date','asc')->get();
        $stats = [];
        $date_prev = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $operations[0]->date);
        $date_prev->hour = 0;
        $date_prev->minute = 0;
        $date_prev->second = 0;
        $data_iter = 0;
        foreach ($operations as $operation){

            if(!isset($stats['data'])){
                $stats['data'][$operation->source->id][$data_iter] = 0;
            }
            if(!isset($stats['data'][$operation->source->id])){
                $stats['data'][$operation->source->id][$data_iter] = 0;
            }

            $date = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $operation->date);
            $date->hour = 0;
            $date->minute = 0;
            $date->second = 0;

            $prev = $date_prev->format('Y-m-d H:i:s');
            $cur = $date->format('Y-m-d H:i:s');

            if($date->diffInMonths($date_prev)>=1){
                $data_iter++;
                $date_prev = $date;
                $stats['data'][$operation->source->id][$data_iter] = 0;
            }
            $stats['data'][$operation->source->id][$data_iter] += $operation->amount;
            $stats['labels'][] = $date->format('F');
            $stats['series'][] = $operation->source->name;
        }

        foreach ($stats['data'] as $key => $source){
            for ($i=1;$i<count($source);$i++){
                $stats['data'][$key][$i]=$source[$i-1];
            }
        }

        $stats['labels'] = array_keys(array_flip($stats['labels']));
        $stats['series'] = array_keys(array_flip($stats['series']));
        return $stats;
    }

}