<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use Auth;
use Input;
use Validator;
use Hash;
use DB;

use App\Models\User;

class ProfileController extends Controller {



    /**
     *
     *   editProfile
     *       - Update an Existing User
     *
     *   Params ($_POST):
     *       - firstname:                (String) The User Firstname
     *       - lastname:                 (String) The User Lastname
     *       - username:                 (String) The User's Username
     *       - password:                 (String) The User's Password
     *       - password_confirmation     (String) A Password Confirmation
     *       - city:                     (String) The User's City
     *       - province:                 (String) The User's Province
     *       - phone:                    (String) The User's Phone
     *       - gender:                   (String) The User's Gender
     *       - attachment_id:            (INT) The User's Attachment ID
     *
     *    Returns (Array):
     *        1. (Bool) Returns True / False
     *
     **/
    public function editProfile(){

        return UserController::editUser([
            'id'        => Auth::user()->id
        ]);

    }



    /**
     *
     *   getProfile
     *       - load current User with Roles and Permissions
     *
     *   Params ($_POST):
     *
     *   Returns (JSON):
     *       2. The Current User Session Data (If User authorized)
     *       3. Null
     *
     **/
    public function getProfile(){

        if (Auth::check()) {

            if( $user = User::where('id' , '=', Auth::user()->id )->with('roles')->first() ){

                $permissions = $user->getPermissions()->pluck('slug')->toArray();
                $data = $user->toArray();
                $data['permissions'] = $permissions;
                //Return the Data
                return [
                    'result' => 1,
                    'data'   => $data,
                ];

            }else{

                //Return Failure
                return [ 'result' => 0 , 'errors' => [ 'That User doesn\'t exist' ] ];

            }

        } else {

            return [
                'result' => 1,
                'data'   => [],
            ];

        }

    }



    /**
     *
     *   redirectToProvider
     *       - redirect to social network provider
     *
     *   Params ($provider):
     *       - provider:                (String) Name of social network
     *
     *    Returns (Object):
     *        1. (Object) Returns params of redirect responce
     *
     **/
    public function redirectToProvider( $provider ) {
        return \Socialite::driver( $provider )->redirect();
    }



    /**
     *
     *   handleProviderCallback
     *       -
     *
     *   Params ($provider):
     *       - provider:                (String) Name of social network
     *
     *    Returns ():
     *        1. Redirects to login state
     *
     **/
    public function handleProviderCallback ( $provider ) {
        $user = \Socialite::driver($provider)->user();

        //Check if user is logged in
        if ($authUser = Auth::user()) {
            $authUser = User::find($authUser->id);
        }

        if ($provider == "twitter") {

            if ($authUser != null) {
                //set twitter id for auth user
                $authUser->twitter_id = $user->id;
            }
            //rows for new user
            $newUser = [
                'firstname' => $user->name,
                'lastname' => 'Test',
                'username' => $user->nickname,
                'email' => 'test@test.test' . time(),
                'password' => 'Password',
                'password_confirmation' => 'Password',
                'twitter_id' => $user->id
            ];

        } else if ($provider == "facebook") {

            if ($authUser != null) {
                //set facebook id for auth user
                $authUser->facebook_id = $user->id;
            }
            //rows for new user
            $name = explode(" ", $user->name);
            $newUser = [
                'firstname' => $name[0],
                'lastname' => $name[count($name)-1],
                'username' => $user->email,
                'email' => $user->email,
                'password' => 'Password',
                'password_confirmation' => 'Password',
                'facebook_id' => $user->id
            ];

        }

        // if User isn't logged in
        if ($authUser == null) {

            //find user with these social id and log in if he's found
            if ($provider == 'twitter') {
                $authUser = User::where( 'twitter_id', '=' , $user->id )->first();
            } else if ($provider == 'facebook') {
                $authUser = User::where( 'facebook_id', '=' , $user->id )->first();
            }
            if ($authUser) {
                Auth::login($authUser, true);
                return \Redirect::to('/');
            } else {
                //this social id is new so create a new profile
                DB::beginTransaction();

                try {
                    $authUser = $this->findOrCreateUser($newUser, $provider);
                    DB::commit();
                    Auth::login($authUser, true);
                    return \Redirect::to('/');
                } catch (\Exception $e) {

                    DB::rollback();
                    //Return Failure
                    return ['result' => 0, 'errors' => [$e->getMessage()]];

                }
            }

        } else if (isset($authUser->id)) {
            //if User is logged in and wants to attach social acc
            if ($provider == 'twitter') {
                $existUser = User::where( 'twitter_id', '=' , $user->id )->first();
            } else if ($provider == 'facebook') {
                $existUser = User::where( 'facebook_id', '=' , $user->id )->first();
            }
            if ($existUser == null) {
                //add social id
                DB::beginTransaction();

                try {
                    $authUser->save();
                    DB::commit();
                    return \Redirect::to('/profile');
                } catch (\Exception $e) {

                    DB::rollback();
                    //Return Failure
                    return ['result' => 0, 'errors' => [$e->getMessage()]];

                }
            } else {
                //account with these social id already exist
                return dd('user already exist!');
            }

        }

        //Return Failure
        return [ 'result' => 0 , 'errors' => [ 'Smth goes wrong' ] ];

    }



    /**
     *
     *   createProfile
     *       - Create User
     *
     *   Params ($_POST):
     *       - firstname:                (String) The User Firstname
     *       - lastname:                 (String) The User Lastname
     *       - username:                 (String) The User's Username
     *       - password:                 (String) The User's Password
     *       - password_confirmation     (String) A Password Confirmation
     *       - city:                     (String) The User's City
     *       - province:                 (String) The User's Province
     *       - phone:                    (String) The User's Phone
     *       - gender:                   (String) The User's Gender
     *
     *    Returns (Array):
     *        1. (Bool) Returns True / False
     *
     **/
    public function createProfile(){

        return UserController::addUser();

    }


    /**
     *
     *   findOrCreateUser
     *       - find existing User Or Create a new one
     *
     *   Params ($user):
     *       - firstname:                (String) The User Firstname
     *       - lastname:                 (String) The User Lastname
     *       - username:                 (String) The User's Username
     *       - password:                 (String) The User's Password
     *       - password_confirmation     (String) A Password Confirmation
     *
     *    Returns (Array):
     *        1. (Array) Returns $user
     *
     **/
    public function findOrCreateUser($user){

        $authUser = UserController::findUser($user['username'], $user['email'])['data'];
        if ($authUser) {
            return $authUser;
        }

        UserController::addUser($user);
        return UserController::findUser($user['username'], $user['email'])['data'];

    }

}