<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use Illuminate\Database\Eloquent\Model;
use DB;
use Input;
use Validator;

abstract class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * @var Model
     */
    public $model;

    /**
     * get Model
     * @param int $limit The Page Limit (Default: 15)
     * @param int $page Pages to Load (Default: 1)
     * @return array
     */
    public function getPaged($limit = 0, $page = 1)
    {

        if ($limit > 0) {

            $query = $this->model->take($limit)->skip((($page - 1) * $limit));

            $total = $this->model->count();
            $data = $query->get();

            //Return the Data
            return [
                'result' => 1,
                'total' => $total,
                'data' => $data
            ];

        } else if ($limit <= 0) {

            //Get the All Models
            $data = $this->model->get();

            //Return Result
            return [
                'result' => 1,
                'data' => $data
            ];

        } else {

            //Return Failure
            return ['result' => 0, 'errors' => ['That Model doesn\'t exist']];

        }

    }

    /**
     * get Model by id
     * @param int $id Model id
     * @param array $with
     * @return array
     */
    public function get($id, array $with = [])
    {

        $data['id'] = $id;

        $validator_rules = [
            'id' => "required|integer|exists:" . $this->model->table . ",id",
        ];

        $validator = Validator::make($data, $validator_rules);

        if ($validator->fails()) {

            //Return Failure
            return ['result' => 0, 'errors' => $validator->errors()->all()];

        } else {
            try {

                $model = $this->model;
                if(count($with)>0){
                    $model = call_user_func_array([$model, 'with'], $with);
                }
                //Get the Model
                if ($model = $model->find($data['id'])) {

                    //Return Result
                    return [
                        'data' => $model,
                        'result' => 1,
                    ];

                }

                //Return Failed
                return ['result' => 0, 'errors' => ['That Model Doesn\'t exist']];

            } catch (Exception $e) {

                //Return Failure
                return ['result' => 0, 'errors' => [$e->getMessage()]];

            }

        }

    }

    /**
     * edit Model
     * @return array
     */
    public function edit()
    {

        $data = call_user_func_array(['Input', 'only'], array_diff(
            array_merge($this->model->getFillable(),$this->model->getAttributes()), $this->model->getHidden()
        ));

        $validator_rules = $this->model->validator_rules;

        if (isset($data['id'])) {
            $validator_rules = [
                'id' => "required|integer|exists:" . $this->model->table . ",id",
            ];
        }

        $validator = Validator::make($data, $validator_rules);

        if ($validator->fails()) {

            //Return Failure
            return ['result' => 0, 'errors' => $validator->errors()->all()];

        } else {

            DB::beginTransaction();
            try {

                //$this->model->updateOrCreate($data);

                if (isset($data['id'])) {
                    //Get the Model
                    $model = $this->model->find($data['id']);
                    $model->fill($data);
                    $model->save();

                } else {
                    $this->model->create($data);
                }

                DB::commit();
                //Return Success
                return ['result' => 1];

            } catch (Exception $e) {

                DB::rollback();
                //Return Failure
                return ['result' => 0, 'errors' => [$e->getMessage()]];

            }

        }

    }

    /**
     * delete Model
     * @param $id
     * @return array
     */
    public function delete($id)
    {

        $validator = Validator::make(
            [
                'id' => $id
            ],
            [
                'id' => "required|integer|exists:" . $this->model->table . ",id",
            ]
        );

        if ($validator->fails()) {

            //Return Failure
            return ['result' => 0, 'errors' => $validator->errors()->all()];

        } else {

            DB::beginTransaction();
            try {

                $this->model->find($id)->delete();

                DB::commit();
                //Return Success
                return ['result' => 1];

            } catch (Exception $e) {

                DB::rollback();
                //Return Failure
                return ['result' => 0, 'errors' => [$e->getMessage()]];

            }

        }

    }
}
