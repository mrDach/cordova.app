var gulp = require('gulp');
var gutil = require('gulp-util');
var bower = require('bower');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var minifyCss = require('gulp-minify-css');
var rename = require('gulp-rename');
var sh = require('shelljs');
var watch = require('gulp-watch');
var wiredep = require('wiredep').stream;
var mainBowerFiles = require('gulp-main-bower-files');
var uglify = require('gulp-uglify');
var gulpFilter = require('gulp-filter');

var paths = {
    sass:   ['./src/scss/**/*.scss'],
    js:     ['./src/js/**/*'],
    images: ['./src/images/**/*']
};

var assets = './src/';
var config = {
    sassResources:      assets+'sass/',
    jsResources:        assets+'js/',
    imagesResources:    assets+'images/',
    layoutsResources:   './resources/views/layouts/',
    bowerDir:           './www/lib/'
};

gulp.task('vendor', function () {
    gulp.src('./src/scss/style.scss')
        .pipe(wiredep())
        .pipe(gulp.dest('./src/scss/'));

    gulp.src('./www/index.html')
        .pipe(wiredep())
        .pipe(gulp.dest('./www/'));
});

gulp.task('components', function () {
    return gulp.src('./src/components/**/*')
        .pipe(gulp.dest('./www/components'));
});

gulp.task('components-watch', function () {
    return gulp.src('./src/components/**/*', {base: './src/components/'})
        .pipe(watch('./src/components/', {base: './src/components/'}))
        .pipe(gulp.dest('./www/components/'));
});

gulp.task('images', function () {
    return gulp.src('./src/images/**/*')
        .pipe(gulp.dest('./www/images'));
});

gulp.task('fonts', function () {
    //Copy Fonts
    //TODO: make more usable resolution
    gulp.src('./src/fonts/*')
        .pipe(gulp.dest('./www/fonts'));
    gulp.src(config.bowerDir+'font-awesome/fonts/**/*')
        .pipe(gulp.dest('./www/fonts'));
    gulp.src(config.bowerDir+'bootstrap-sass/assets/fonts/**/*')
        .pipe(gulp.dest('./www/fonts'));
});

gulp.task('sass', function (done) {
    return gulp.src('./src/scss/style.scss')
        .pipe(sass())
        .on('error', sass.logError)
        .pipe(gulp.dest('./www/css/'))
        .pipe(minifyCss({
            keepSpecialComments: 0
        }))
        .pipe(rename({extname: '.min.css'}))
        .pipe(gulp.dest('./www/css/'));
});

gulp.task('default', ['vendor', 'sass', 'images', 'fonts', 'components']);

gulp.task('watch', ['default', 'components-watch'], function () {
    gulp.watch(paths.sass, ['sass']);
});

gulp.task('install', ['git-check'], function () {
    return bower.commands.install()
        .on('log', function (data) {
            gutil.log('bower', gutil.colors.cyan(data.id), data.message);
        });
});

gulp.task('git-check', function (done) {
    if (!sh.which('git')) {
        console.log(
            '  ' + gutil.colors.red('Git is not installed.'),
            '\n  Git, the version control system, is required to download Ionic.',
            '\n  Download git here:', gutil.colors.cyan('http://git-scm.com/downloads') + '.',
            '\n  Once git is installed, run \'' + gutil.colors.cyan('gulp install') + '\' again.'
        );
        process.exit(1);
    }
    done();
});
